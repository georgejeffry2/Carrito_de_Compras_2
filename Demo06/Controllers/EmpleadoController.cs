﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Northwind.Librerias.EntidadesNegocio;
using Northwind.Librerias.ReglasNegocio;

namespace Demo04.Controllers
{
    public class EmpleadoController : Controller
    {
        private int registroPagina = 4;
        private List<beEmpleado> lbeEmpleado;
        List<beEmpleado> lbePagina;
        private int indicePaginaActual;
        private int indiceUltimaPagina;

        private int indicePaginaAnterior;

        public ActionResult Lista(string pagina)
        {
            //List<beEmpleado> lbeEmpleado = null;
            if (Session["Empleados"] == null)
            {
                brEmpleado obrEmpleado = new brEmpleado();
                lbeEmpleado = obrEmpleado.Listar();
                Session["Empleados"] = lbeEmpleado;
                indicePaginaActual = 0;

                indicePaginaAnterior = 0;
            }
            else
            {
                lbeEmpleado = (List<beEmpleado>)Session["Empleados"];

            }

            indiceUltimaPagina = lbeEmpleado.Count / registroPagina;
            if (lbeEmpleado.Count % registroPagina == 0) indiceUltimaPagina--;
            ViewBag.IndiceUltimaPagina = indiceUltimaPagina;



            if (Session["indicePaginaActual"] != null)
                indicePaginaActual = (int)Session["indicePaginaActual"];

            Session["indicePaginaAnterior"] = indicePaginaActual;

            if (pagina != null)
            {

               
                switch (pagina)
                {
                    case "<<":
                        indicePaginaActual = 0;
                        break;
                    case "<":
                        if (indicePaginaActual > 0) indicePaginaActual--;
                        break;
                    case ">":
                        if (indicePaginaActual < indiceUltimaPagina) indicePaginaActual++;
                        break;
                    case ">>":
                        indicePaginaActual = indiceUltimaPagina;
                        break;
                    default:
                        indicePaginaActual = int.Parse(pagina);
                        break;
                }
               
                
            }
            ViewBag.Posicion = string.Format("{0} de {1}", indicePaginaActual + 1, indiceUltimaPagina + 1);

            if (TempData["campo"] != null)
            {
                string campo = TempData["campo"].ToString();
                ViewData[campo] = TempData["simbolo"];
            }

            Session["indicePaginaActual"] = indicePaginaActual;

            return View(mostrarPagina());
        }

       
        private List<beEmpleado> mostrarPagina()
        {
            if (Session["indicePaginaActual"] == null || Session["DatosMostrados"] == null || (int)Session["indicePaginaActual"] != (int)Session["indicePaginaAnterior"])
            {

                int inicio = indicePaginaActual * registroPagina;
                int fin = inicio + registroPagina;
                lbePagina = new List<beEmpleado>();
                for (int i = inicio; i < fin; i++)
                {
                    if (i < lbeEmpleado.Count) lbePagina.Add(lbeEmpleado[i]);
                    else break;
                }

                Session["DatosMostrados"] = lbePagina;
            }
            else
            {
               
                lbePagina = (List<beEmpleado>)Session["DatosMostrados"];
            }
                        

            return lbePagina;
        }

        public ActionResult Ordenar(string campo)
        {
            //lbeEmpleado = (List<beEmpleado>)Session["Empleados"];
            lbePagina = (List<beEmpleado>)Session["DatosMostrados"];
            int n = 0;

            string simbolo = "▲";

            if (TempData[campo] != null)
            {
                if (TempData[campo].Equals(0))
                {
                    n = 1;
                    simbolo = "▼";
                }
            }
            TempData[campo] = n;
            TempData["campo"] = campo;
            TempData["simbolo"] = simbolo;

            if (n == 0) lbePagina = lbePagina.OrderBy(x => x.GetType().GetProperty(campo).GetValue(x, null)).ToList();
            else lbePagina = lbePagina.OrderByDescending(x => x.GetType().GetProperty(campo).GetValue(x, null)).ToList();

            Session["DatosMostrados"] = lbePagina;

            return RedirectToAction("Lista");
        }
	}
}