﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using General.Librerias.EntidadesNegocio;
using Northwind.Librerias.EntidadesNegocio;

namespace Northwind.Librerias.AccesoDatos
{
    public class daProducts
    {
        public List<beProducts> listar(SqlConnection con)
        {
            List<beProducts> lbeProducts = null;
            SqlCommand cmd = new SqlCommand("uspProductsListar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader drd = cmd.ExecuteReader(CommandBehavior.SingleResult);
            if (drd != null)
            {
                lbeProducts = new List<beProducts>();
                int posProductID = drd.GetOrdinal("ProductID");
                int posProductName = drd.GetOrdinal("ProductName");
                int posSupplierID = drd.GetOrdinal("SupplierID");
                int posCategoryID = drd.GetOrdinal("CategoryID");
                int posUnitPrice = drd.GetOrdinal("UnitPrice");
                int posUnitsInStock = drd.GetOrdinal("UnitsInStock");

                beProducts obeProducts;
                while (drd.Read())
                {
                    obeProducts = new beProducts();
                    obeProducts.ProductID = drd.GetInt32(posProductID);
                    obeProducts.ProductName = drd.GetString(posProductName);
                    obeProducts.SupplierID = drd.GetInt32(posSupplierID);
                    obeProducts.CategoryID = drd.GetInt32(posCategoryID);
                    obeProducts.UnitPrice = drd.GetDecimal(posUnitPrice);
                    obeProducts.UnitsInStock = drd.GetInt16(posUnitsInStock);
                    lbeProducts.Add(obeProducts);
                }
                drd.Close();
            }
            return (lbeProducts);
        }

        public beProductsListas obtenerListas(SqlConnection con)
        {
            beProductsListas obeProductsListas = new beProductsListas();
            List<beProducts> lbeProducts = null;
            SqlCommand cmd = new SqlCommand("uspProductsObtenerListas", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader drd = cmd.ExecuteReader();
            if(drd!=null)
            {
                lbeProducts = new List<beProducts>();
                int posProductID = drd.GetOrdinal("ProductID");
                int posProductName = drd.GetOrdinal("ProductName");
                int posSupplierID = drd.GetOrdinal("SupplierID");
                int posCategoryID = drd.GetOrdinal("CategoryID");
                int posUnitPrice = drd.GetOrdinal("UnitPrice");
                int posUnitsInStock = drd.GetOrdinal("UnitsInStock");
                beProducts obeProducts;
                while (drd.Read())
                {
                    obeProducts = new beProducts();
                    obeProducts.ProductID = drd.GetInt32(posProductID);
                    obeProducts.ProductName = drd.GetString(posProductName);
                    obeProducts.SupplierID = drd.GetInt32(posSupplierID);
                    obeProducts.CategoryID = drd.GetInt32(posCategoryID);
                    obeProducts.UnitPrice = drd.GetDecimal(posUnitPrice);
                    obeProducts.UnitsInStock = drd.GetInt16(posUnitsInStock);
                    lbeProducts.Add(obeProducts);
                }
                obeProductsListas.ListaProducts = lbeProducts;
                List<beCampoEntero> lbeSuppliers = new List<beCampoEntero>();
                if (drd.NextResult())
                {
                    beCampoEntero obeSuppliers;
                    while (drd.Read())
                    {
                        obeSuppliers = new beCampoEntero();
                        obeSuppliers.Campo1 = drd.GetInt32(0);
                        obeSuppliers.Campo2 = drd.GetString(1);
                        lbeSuppliers.Add(obeSuppliers);
                    }
                    obeProductsListas.ListaSuppliers = lbeSuppliers;
                }
                List<beCampoEntero> lbeCategories = new List<beCampoEntero>();
                if (drd.NextResult())
                {
                    beCampoEntero obeCategories;
                    while (drd.Read())
                    {
                        obeCategories = new beCampoEntero();
                        obeCategories.Campo1 = drd.GetInt32(0);
                        obeCategories.Campo2 = drd.GetString(1);
                        lbeCategories.Add(obeCategories);
                    }
                    obeProductsListas.ListaCategories = lbeCategories;
                }

                drd.Close();
            }
            return (obeProductsListas);
        }

    }
}
