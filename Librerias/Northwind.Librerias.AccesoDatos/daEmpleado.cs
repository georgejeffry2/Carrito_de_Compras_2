﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using Northwind.Librerias.EntidadesNegocio;
using System.IO;

namespace Northwind.Librerias.AccesoDatos
{
    public class daEmpleado
    {
        public List<beEmpleado> Listar(SqlConnection con)
        {
            List<beEmpleado> lbeEmpleado = null;
            SqlCommand cmd = new SqlCommand("uspEmployeesListar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader drd = cmd.ExecuteReader(CommandBehavior.SingleResult);

            if (drd != null)
            {
                lbeEmpleado = new List<beEmpleado>();
                beEmpleado obeEmpleado;
                int posIdEmpleado = drd.GetOrdinal("EmployeeID");
                int posApellido = drd.GetOrdinal("LastName");
                int posNombre = drd.GetOrdinal("FirstName");
                int posFechaNacimiento = drd.GetOrdinal("BirthDate");

                while (drd.Read())
                {
                    obeEmpleado = new beEmpleado();

                    obeEmpleado.IdEmpleado = drd.GetInt32(posIdEmpleado);
                    obeEmpleado.Apellido = drd.GetString(posApellido);
                    obeEmpleado.Nombre = drd.GetString(posNombre);
                    obeEmpleado.FechaNacimiento = drd.GetDateTime(posFechaNacimiento);

                    string ruta = @"C:\Users\Administrador\Desktop\Proyecto Clases\Prueba\Demo06\Images\Empleados\";
                    string archivo = string.Format("{0}{1}.jpg", ruta, obeEmpleado.IdEmpleado);
                    if (!File.Exists(archivo)) archivo = string.Format("{0}No.png", ruta);
                    obeEmpleado.Foto = File.ReadAllBytes(archivo);

                    lbeEmpleado.Add(obeEmpleado);
                }
            }

            drd.Close();

            return (lbeEmpleado);
        }



        public beEmpleado ObtenerPorId(SqlConnection con, int idEmpleado)
        {
            beEmpleado obeEmpleado = null;
            SqlCommand cmd = new SqlCommand("uspEmployeesObtenerPorId", con);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter par = cmd.Parameters.Add("@EmployeeID", SqlDbType.Int);
            par.Direction = ParameterDirection.Input;
            par.Value = idEmpleado;

            SqlDataReader drd = cmd.ExecuteReader(CommandBehavior.SingleRow);

            if (drd != null)
            {
                if (drd.HasRows)
                {
                    int posIdEmpleado = drd.GetOrdinal("EmployeeID");
                    int posApellido = drd.GetOrdinal("LastName");
                    int posNombre = drd.GetOrdinal("FirstName");
                    int posFechaNacimiento = drd.GetOrdinal("BirthDate");

                    drd.Read();                
                    obeEmpleado = new beEmpleado();
                    obeEmpleado.IdEmpleado = drd.GetInt32(posIdEmpleado);
                    obeEmpleado.Apellido = drd.GetString(posApellido);
                    obeEmpleado.Nombre = drd.GetString(posNombre);
                    obeEmpleado.FechaNacimiento = drd.GetDateTime(posFechaNacimiento);

                    string ruta = @"I:\Cod\C#_cod_Proy_Jeff\Web\CIPSA\Demo08\Demo08\Images\Empleados\";
                    string archivo = string.Format("{0}{1}.jpg",ruta,obeEmpleado.IdEmpleado);
                    if (!File.Exists(archivo)) archivo = string.Format("{0}No.png",ruta);
                    obeEmpleado.Foto = File.ReadAllBytes(archivo);
                }
                drd.Close();
            }

            return (obeEmpleado);
        }
    }
}
