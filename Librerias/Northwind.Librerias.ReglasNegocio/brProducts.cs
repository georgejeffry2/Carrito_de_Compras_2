﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Northwind.Librerias.EntidadesNegocio;
using Northwind.Librerias.AccesoDatos;
using General.Librerias.CodigoUsuario;
using General.Librerias.EntidadesNegocio;


namespace Northwind.Librerias.ReglasNegocio
{
    public class brProducts:brGeneral
    {
        public List<beProducts> listar()
        {
            List<beProducts> lbeProducts = null;
            using (SqlConnection con = new SqlConnection(CadenaConexion))
            {
                try
                {
                    con.Open();
                    daProducts odaProducts = new daProducts();
                    lbeProducts = odaProducts.listar(con);
                }
                catch (SqlException ex)
                {
                    foreach (SqlError err in ex.Errors)
                    {
                        ucObjeto.Grabar<SqlError>(err, err.Message);
                    }
                }
                catch (Exception ex)
                {
                    ucObjeto.Grabar<Exception>(ex, ex.Message);
                }
            }
            return (lbeProducts);
        }

        //public beProducts obtenerPorId(int ProductID)
        //{
        //    beProducts obeProducts = null;
        //    using(SqlConnection con =new SqlConnection(CadenaConexion))
        //    {
        //        try
        //        {
        //            con.Open();
        //            daProducts odaProducts = new daProducts();
        //            obeProducts = odaProducts.obtenerPorId(con, ProductID);

        //        }
        //        catch (SqlException ex)
        //        {
        //            foreach (SqlError err in ex.Errors)
        //            {
        //                ucObjeto.Grabar<SqlError>(err, err.Message);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ucObjeto.Grabar<Exception>(ex, ex.Message);
        //        }
        //    }
        //    return (obeProducts);
        //}

        //public int adicionar(beProducts obeProducts)
        //{
        //    int idProducts = -1;
        //    using (SqlConnection con = new SqlConnection(CadenaConexion))
        //    {
        //        try
        //        {
        //            con.Open();
        //            daProducts odaProducts = new daProducts();
        //            idProducts = odaProducts.adicionar(con, obeProducts);
        //        }
        //        catch (SqlException ex)
        //        {
        //            foreach (SqlError err in ex.Errors)
        //            {
        //                ucObjeto.Grabar<SqlError>(err, err.Message);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ucObjeto.Grabar<Exception>(ex, ex.Message);
        //        }
        //    }
        //    return idProducts;
        //}

        public beProductsListas obtenerListas()
        {
            beProductsListas obeProductsListas = null;
            using (SqlConnection con = new SqlConnection(CadenaConexion))
            {
                try
                {
                    con.Open();
                    daProducts odaProducts = new daProducts();
                    obeProductsListas = odaProducts.obtenerListas(con);
                }
                catch (SqlException ex)
                {
                    foreach (SqlError err in ex.Errors)
                    {
                        ucObjeto.Grabar<SqlError>(err, err.Message);
                    }
                }
                catch (Exception ex)
                {
                    ucObjeto.Grabar<Exception>(ex, ex.Message);
                }
            }
            return (obeProductsListas);
        }
    }
}
