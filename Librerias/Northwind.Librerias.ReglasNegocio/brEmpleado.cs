﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using Northwind.Librerias.EntidadesNegocio;
using Northwind.Librerias.AccesoDatos;

namespace Northwind.Librerias.ReglasNegocio
{
    public class brEmpleado:brGeneral
    {
        public List<beEmpleado> Listar()
        {
            List<beEmpleado> lbeEmpleado = null;
           
            using (SqlConnection con = new SqlConnection(CadenaConexion))
            {
                try
                {
                    con.Open();
                    daEmpleado odaEmpleado = new daEmpleado();
                    lbeEmpleado = odaEmpleado.Listar(con);
                }
                catch (SqlException ex)
                {
                    foreach (SqlError err in ex.Errors)
                    {
                        //GrabarLog
                    }
                }
                catch (Exception ex)
                { 
                    //GrabarLog
                }
            }//ocn.close(); con.Dispose();
            
            return (lbeEmpleado); 
        }

        public beEmpleado ObtenerPorId(int idEmpleado)
        {
            beEmpleado obeEmpleado = null;

            using (SqlConnection con = new SqlConnection(CadenaConexion))
            {
                try
                {
                    con.Open();
                    daEmpleado odaEmpleado = new daEmpleado();
                    obeEmpleado = odaEmpleado.ObtenerPorId(con, idEmpleado);
                }
                catch (SqlException ex)
                {
                    foreach (SqlError err in ex.Errors)
                    {
                        //GrabarLog
                    }
                }
                catch (Exception ex)
                {
                    //GrabarLog
                }
            }//ocn.close(); con.Dispose();

            return (obeEmpleado);
        }

    }
}
