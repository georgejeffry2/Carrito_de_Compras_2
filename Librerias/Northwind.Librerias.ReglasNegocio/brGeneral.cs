﻿using System;
using System.Configuration;

namespace Northwind.Librerias.ReglasNegocio
{
    public class brGeneral
    {
        public string CadenaConexion { get; set; }
        public brGeneral()
        {
            CadenaConexion = ConfigurationManager.ConnectionStrings["conNW"].ConnectionString;
        }
    }
}
