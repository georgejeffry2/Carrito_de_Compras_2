﻿using System;
using System.Web;//HttpContext
using System.IO;

namespace General.Librerias.CodigoWeb
{
    public class Imagen
    {
        public static string obtenerUrl(string carpeta, string id)
        {
            string url = string.Format("Content/Imagenes/{0}/{1}.jpg", carpeta, id);
            string archivo = HttpContext.Current.Server.MapPath(url);
            if (!File.Exists(archivo)) url = string.Format("Content/Imagenes/{0}/No.png", carpeta);
            return url;
        }
    }
}
