﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Librerias.CodigoWeb
{
    public class HtmlFormat
    {
        public static string formatearColumna(string valor, string dato)
        {
            string rpta = valor;

            if (dato!=null && !dato.Equals("")) rpta = valor.Replace(dato, string.Format("<span class='Coincidencia'>{0}</span>", dato));

            return rpta;
        }
    }
}
