﻿using System;
using System.Collections.Generic;
using General.Librerias.EntidadesNegocio;

namespace Northwind.Librerias.EntidadesNegocio
{
    public class beProductsListas
    {
        public List<beProducts> ListaProducts { get; set; }
        public List<beCampoEntero> ListaSuppliers { get; set; }
        public List<beCampoEntero> ListaCategories { get; set; }
    }
}
