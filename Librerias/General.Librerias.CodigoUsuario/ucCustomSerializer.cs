﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Reflection;

namespace General.Librerias.CodigoUsuario
{
    public class ucCustomSerializer
    {
        public static string Serialize<T>(List<T> lista,char separadorCampo,char separadorRegistro)
        {
            StringBuilder sb = new StringBuilder();            
            if(lista!=null&&lista.Count>0)
            {
                //Grabar Cabeceras
                PropertyInfo[] propiedades = lista[0].GetType().GetProperties();
                foreach (PropertyInfo propiedad in propiedades)
                {
                    sb.Append(propiedad.Name);
                    sb.Append(separadorCampo);
                }
                sb = sb.Remove(sb.Length - 1, 1);
                sb.Append(separadorRegistro);
                //Grabar los Datos
                object valor;
                foreach (T obj in lista)
                {
                    propiedades = obj.GetType().GetProperties();
                    foreach (PropertyInfo propiedad in propiedades)
                    {
                        valor = propiedad.GetValue(obj,null);
                        if(valor!=null) sb.Append(valor.ToString());
                        else sb.Append("");
                        sb.Append(separadorCampo);
                    }
                    sb = sb.Remove(sb.Length - 1, 1);
                    sb.Append(separadorRegistro);
                }
                sb = sb.Remove(sb.Length - 1, 1);
            }
            return sb.ToString();
        }

        public static string Serialize<T>(T obj, char separadorCampo)
        {
            StringBuilder sb = new StringBuilder();
            //Grabar Cabeceras
            PropertyInfo[] propiedades = obj.GetType().GetProperties();
            object valor;
            foreach (PropertyInfo propiedad in propiedades)
            {
                valor = propiedad.GetValue(obj, null);
                if (valor != null) sb.Append(valor.ToString());
                else sb.Append("");
                sb.Append(separadorCampo);
            }
            sb = sb.Remove(sb.Length - 1, 1);
            return sb.ToString();
        }

        public static List<T> Deserialize<T>(string data, char separadorCampo, char separadorRegistro)
        {
            List<T> lista = new List<T>();
            if (!String.IsNullOrWhiteSpace(data))
            {
                string[] registros = data.Split(separadorRegistro);
                string[] cabeceras=registros[0].Split(separadorCampo);
                string[] campos;
                Type tipo;
                object obj;
                T objTipo;
                dynamic valor = null;
                Type tipoPropiedad;
                PropertyInfo propiedad;
                int nreg = registros.Length;
                int ncampos = cabeceras.Length;
                for (int i = 1; i < nreg; i++)
                {
                    tipo = typeof(T);
                    obj = Activator.CreateInstance(tipo);
                    if (obj != null)
                    {
                        objTipo = (T)Convert.ChangeType(obj, tipo);
                        campos = registros[i].Split(separadorCampo);

                        for (int j = 0; j < ncampos; j++)
                        {
                            propiedad = obj.GetType().GetProperty(cabeceras[j]);
                            if (propiedad != null)
                            {
                                tipoPropiedad = propiedad.PropertyType;
                                valor = Convert.ChangeType(campos[j], tipoPropiedad);
                                propiedad.SetValue(objTipo, valor);
                            }
                        }
                        lista.Add(objTipo);
                    }
                }
            }
            return lista;
        }
    }
}
