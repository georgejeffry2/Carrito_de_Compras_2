﻿using System;
using System.Text; //Encoding
using System.Data.OleDb;
using System.Collections.Generic;
using System.Reflection; //PropertyInfo
using System.IO; //StreamWriter
using General.Librerias.EntidadesNegocio;

namespace General.Librerias.CodigoUsuario
{
    public class ucExportacion
    {
        //public static void exportarTxt<T>(string archivo, List<T> lista,string separador=",", List<beDataGridColumn> Columnas = null)
        //{
        //    if (lista != null & lista.Count > 0)
        //    {
        //        using (StreamWriter sw = new StreamWriter(archivo, false, Encoding.Default))
        //        {
        //            if (Columnas == null)
        //            {
        //                //Escribir todos los nombres de Campos como Cabeceras
        //                PropertyInfo[] propiedades = lista[0].GetType().GetProperties();
        //                for (int i = 0; i < propiedades.Length - 1; i++)
        //                {
        //                    sw.Write(propiedades[i].Name);
        //                    sw.Write(separador);
        //                }
        //                sw.WriteLine(propiedades[propiedades.Length - 1].Name);
        //                //Escribir todos los campos de todos los registros
        //                string tipo;
        //                foreach (T obe in lista)
        //                {
        //                    propiedades = obe.GetType().GetProperties();
        //                    for (int i = 0; i < propiedades.Length - 1; i++)
        //                    {
        //                        tipo = propiedades[i].PropertyType.ToString();
        //                        if (tipo.Contains("DateTime")) sw.Write(((DateTime)propiedades[i].GetValue(obe, null)).ToShortDateString());
        //                        else sw.Write(propiedades[i].GetValue(obe, null).ToString());
        //                        sw.Write(separador);
        //                    }
        //                    tipo = propiedades[propiedades.Length - 1].PropertyType.ToString();
        //                    if (tipo.Contains("DateTime")) sw.Write(((DateTime)propiedades[propiedades.Length - 1].GetValue(obe, null)).ToShortDateString());
        //                    else sw.Write(propiedades[propiedades.Length - 1].GetValue(obe, null).ToString());
        //                    sw.WriteLine();
        //                }
        //            }
        //            else
        //            {
        //                //Escribir solo los nombres de campos configurados como cabeceras
        //                PropertyInfo propiedad;
        //                beDataGridColumn Columna;
        //                for (int i = 0; i < Columnas.Count - 1; i++)
        //                {
        //                    Columna = Columnas[i];
        //                    propiedad = lista[0].GetType().GetProperty(Columna.NombreCampo);
        //                    if (propiedad != null)
        //                    {
        //                        sw.Write(Columna.TextoCabecera);
        //                        sw.Write(separador);
        //                    }
        //                }
        //                Columna = Columnas[Columnas.Count - 1];
        //                propiedad = lista[0].GetType().GetProperty(Columna.NombreCampo);
        //                if (propiedad != null) sw.WriteLine(Columna.TextoCabecera);
        //                //Escribir solo los campos configurados de todos los registros
        //                foreach (T obe in lista)
        //                {
        //                    for (int i = 0; i < Columnas.Count - 1; i++)
        //                    {
        //                        Columna = Columnas[i];
        //                        propiedad = obe.GetType().GetProperty(Columna.NombreCampo);
        //                        if (propiedad != null)
        //                        {
        //                            sw.Write(propiedad.GetValue(obe, null).ToString());
        //                            sw.Write(separador);
        //                        }
        //                    }
        //                    Columna = Columnas[Columnas.Count - 1];
        //                    propiedad = obe.GetType().GetProperty(Columna.NombreCampo);
        //                    if (propiedad != null) sw.WriteLine(propiedad.GetValue(obe, null).ToString());
        //                }
        //            }
        //        }
        //    }
        //}

        //private static void exportarExcel<T>(string archivoTxt, List<T> lista, string hoja, bool eliminar)
        //{
        //    string ruta = Path.GetDirectoryName(archivoTxt);
        //    string nombre = Path.GetFileNameWithoutExtension(archivoTxt);
        //    string archivoXlsx = String.Format("{0}\\{1}.xlsx", ruta, nombre);
        //    if (eliminar)
        //    {
        //        if (File.Exists(archivoXlsx)) File.Delete(archivoXlsx);
        //    }
        //    using (OleDbConnection con = new OleDbConnection("provider=Microsoft.Ace.oledb.12.0;data source=" + ruta + ";extended properties=Text"))
        //    {
        //        con.Open();
        //        OleDbCommand cmd = new OleDbCommand("Select * Into " + hoja + " In ''[Excel 12.0 xml;Database=" + archivoXlsx + "]From " + nombre + "#TXT", con);
        //        cmd.ExecuteNonQuery();
        //    }
        //}

        //public static void exportarXlsx<T>(string archivoTxt, List<T> lista, string nombreHoja,string separador=",", List<beDataGridColumn> Columnas = null)
        //{
        //    if (lista != null & lista.Count > 0)
        //    {
        //        exportarTxt(archivoTxt, lista, separador, Columnas);
        //        crearSchemaIni(archivoTxt, lista[0], separador, Columnas);
        //        exportarExcel(archivoTxt, lista, nombreHoja, true);
        //    }
        //}

        //public static void crearSchemaIni<T>(string archivo, T obe,string separador = ",",List<beDataGridColumn> Columnas = null)
        //{
        //    string nombreArchivo = Path.GetFileName(archivo);
        //    string archivoSchema = Path.Combine(Path.GetDirectoryName(archivo), "Schema.ini");
        //    StringBuilder stbSchema=new StringBuilder();
        //    stbSchema.Append("[");
        //    stbSchema.Append(nombreArchivo);
        //    stbSchema.Append("]");
        //    stbSchema.Append(Environment.NewLine);
        //    stbSchema.Append("ColNameHeader=True");
        //    stbSchema.Append(Environment.NewLine);
        //    stbSchema.Append("Format=Delimited(");
        //    stbSchema.Append(separador);
        //    stbSchema.Append(")");
        //    stbSchema.Append(Environment.NewLine);
        //    stbSchema.Append("MaxScanRows=0");
        //    stbSchema.Append(Environment.NewLine);
        //    stbSchema.Append("CharacterSet=ANSI");
        //    stbSchema.Append(Environment.NewLine);
        //    stbSchema.Append("NumberLeadingZeros = True");
        //    stbSchema.Append(Environment.NewLine);
        //    stbSchema.Append("NumberDigits = 2");
        //    stbSchema.Append(Environment.NewLine);
        //    stbSchema.Append("DateTimeFormat=dd/MM/yyyy hh:nn:ss tt");
        //    //stbSchema.Append("DateTimeFormat=dd/mm/yyyy_hh:nn:ss");
        //    string tipoNET, tipoOledb;
        //    int c = 0;
        //    if (Columnas == null)
        //    {
        //        PropertyInfo[] propiedades = obe.GetType().GetProperties();
        //        foreach (PropertyInfo propiedad in propiedades)
        //        {
        //            c++;
        //            tipoNET=propiedad.PropertyType.ToString();
        //            tipoOledb = obtenerTipoOLEDB(tipoNET);
        //            stbSchema.Append(Environment.NewLine);
        //            stbSchema.Append("Col");
        //            stbSchema.Append(c.ToString());
        //            stbSchema.Append("=");
        //            stbSchema.Append((char)34);
        //            stbSchema.Append(propiedad.Name);
        //            stbSchema.Append((char)34);
        //            stbSchema.Append(" ");
        //            stbSchema.Append(tipoOledb);
        //        }                
        //    }
        //    else
        //    {
        //        beDataGridColumn Columna;
        //        PropertyInfo propiedad;
        //        for (int i = 0; i < Columnas.Count; i++)
        //        {
        //            Columna = Columnas[i];
        //            propiedad = obe.GetType().GetProperty(Columna.NombreCampo);
        //            if (propiedad != null)
        //            {
        //                c++;
        //                tipoNET = propiedad.PropertyType.ToString();
        //                tipoOledb = obtenerTipoOLEDB(tipoNET);
        //                stbSchema.Append(Environment.NewLine);
        //                stbSchema.Append("Col");
        //                stbSchema.Append(c.ToString());
        //                stbSchema.Append("=");
        //                stbSchema.Append((char)34);
        //                stbSchema.Append(Columna.TextoCabecera);
        //                stbSchema.Append((char)34);
        //                stbSchema.Append(" ");
        //                stbSchema.Append(tipoOledb);
        //            }
        //        }
        //    }
        //    File.WriteAllText(archivoSchema, stbSchema.ToString(),Encoding.Default);
        //}

        //private static string obtenerTipoOLEDB(string tipoNET)
        //{
        //    string tipoOledb="";
        //    switch (tipoNET)
        //    {
        //        case "System.String":
        //            tipoOledb = "Text";
        //            break;
        //        case "System.Int8":
        //        case "System.Int16":
        //            tipoOledb = "Integer";
        //            break;
        //        case "System.Int32":
        //        case "System.Int64":
        //        case "System.Single":
        //            tipoOledb = "Single";
        //            break;
        //        case "System.Decimal":
        //        case "System.Double":
        //            tipoOledb = "Double";
        //            break;
        //        case "System.DateTime":
        //        case "System.Nullable`1[System.DateTime]":
        //            tipoOledb = "DateTime";
        //            break;
        //        case "System.Boolean":
        //            tipoOledb = "Bit";
        //            break;
        //        case "System.Char":
        //            tipoOledb = "Char";
        //            break;
        //        default:
        //            tipoOledb = "Text";
        //            break;
        //    }
        //    return (tipoOledb);
        //}
    }
}