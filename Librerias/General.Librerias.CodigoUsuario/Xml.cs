﻿using System;
using System.IO;
using System.Xml;

namespace General.Librerias.CodigoUsuario
{
    public class Xml
    {
        public static string LeerArchivoConfig(string archivo,string atributoBuscado,string valorAtributoBuscado,string atributoDevuelto)
        {
            bool exito = false;
            string valorAtributoDevuelto = "";
            string valorAtributo = "";
            if (File.Exists(archivo))
            {
                XmlTextReader xtr = new XmlTextReader(archivo);
                while (xtr.Read())
                {
                    if (xtr.Name.Equals("add"))
                    {
                        valorAtributo = xtr.GetAttribute(atributoBuscado);
                        if (valorAtributo != null && valorAtributo.Equals(valorAtributoBuscado))
                        {
                            valorAtributoDevuelto = xtr.GetAttribute(atributoDevuelto);
                            if (valorAtributoDevuelto != null)
                            {
                                exito = true;
                                break;
                            }
                        }
                    }
                }
            }
            return valorAtributoDevuelto;
        }
    }
}
