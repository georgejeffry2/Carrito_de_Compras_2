﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Reflection;

namespace General.Librerias.CodigoUsuario
{
    public class CustomSerializer
    {
        public static string Serializar<T>(List<T> lista, char separadorCampo, char separadorRegistro,bool incluirCabeceras=true,string archivo="")
        {
            StringBuilder sb = new StringBuilder();
            if (lista != null && lista.Count > 0)
            {
                PropertyInfo[] propiedades = lista[0].GetType().GetProperties();
                if (archivo == "")
                {
                    if (incluirCabeceras)
                    {
                        for (int i = 0; i < propiedades.Length; i++)
                        {
                            sb.Append(propiedades[i].Name);
                            if (i < propiedades.Length - 1) sb.Append(separadorCampo);
                        }
                        sb.Append(separadorRegistro);
                    }
                    string tipo;
                    object valor;
                    for (int j = 0; j < lista.Count; j++)
                    {
                        propiedades = lista[j].GetType().GetProperties();
                        for (int i = 0; i < propiedades.Length; i++)
                        {
                            tipo = propiedades[i].PropertyType.ToString();
                            valor = propiedades[i].GetValue(lista[j], null);
                            if (valor != null)
                            {
                                if (tipo.Contains("Byte[]"))
                                {
                                    byte[] buffer = (byte[])valor;
                                    sb.Append(Convert.ToBase64String(buffer));
                                }
                                else sb.Append(valor.ToString());
                            }
                            else sb.Append("");
                            if (i < propiedades.Length - 1) sb.Append(separadorCampo);
                        }
                        if (j < lista.Count - 1) sb.Append(separadorRegistro);
                    }
                }
                else
                {
                    if(File.Exists(archivo))
                    {
                        List<string> lineas = File.ReadAllLines(archivo).ToList();
                        List<string> campos = new List<string>();
                        List<string> titulos = new List<string>();
                        List<string> formatos = new List<string>();
                        string[] rpta;
                        foreach (string linea in lineas)
                        {
                            rpta=linea.Split(',');
                            campos.Add(rpta[0]);
                            if (rpta.Length >1) titulos.Add(rpta[1]);
                            else titulos.Add("");
                            if (rpta.Length > 2) formatos.Add(rpta[2]);
                            else formatos.Add("");
                        }
                        List<string> props = new List<string>();
                        for (int i = 0; i < propiedades.Length; i++)
                        {
                            props.Add(propiedades[i].Name);
                        }
                        if (incluirCabeceras)
                        {
                            for (int i = 0; i < campos.Count; i++)
                            {
                                if (props.IndexOf(campos[i]) > -1)
                                {
                                    if(titulos[i]=="") sb.Append(campos[i]);
                                    else sb.Append(titulos[i]);
                                    sb.Append(separadorCampo);
                                }
                            }
                            sb = sb.Remove(sb.Length - 1, 1);
                            sb.Append(separadorRegistro);
                        }
                        string tipo;
                        object valor;
                        for (int j = 0; j < lista.Count; j++)
                        {
                            for (int i = 0; i < campos.Count; i++)
                            {
                                if (props.IndexOf(campos[i]) > -1)
                                {
                                    tipo = lista[j].GetType().GetProperty(campos[i]).PropertyType.ToString();
                                    valor = lista[j].GetType().GetProperty(campos[i]).GetValue(lista[j], null);
                                    if (valor != null)
                                    {
                                        if (tipo.Contains("Byte[]"))
                                        {
                                            byte[] buffer = (byte[])valor;
                                            sb.Append(Convert.ToBase64String(buffer));
                                        }
                                        else
                                        {
                                            if (formatos[i] == "") sb.Append(valor.ToString());
                                            else
                                            {
                                                if (formatos[i] == "Date") sb.Append(String.Format("{0:d}",valor));
                                                else sb.Append(valor.ToString());
                                            }
                                        }
                                    }
                                    else sb.Append("");
                                    sb.Append(separadorCampo);
                                }
                            }
                            sb = sb.Remove(sb.Length - 1, 1);
                            sb.Append(separadorRegistro);
                        }
                        sb = sb.Remove(sb.Length - 1, 1);
                    }
                }
            }
            return sb.ToString();
        }

        public static List<T> Deserializar<T>(string contenido, char separadorCampo,
        char separadorRegistro)
        {
            List<T> lista = new List<T>();
            string[] registros = contenido.Split(separadorRegistro);
            string[] campos;
            string[] cabecera = registros[0].Split(separadorCampo);
            string registro;
            Type tipoObj;
            T obj;
            dynamic valor;
            Type tipoCampo;
            for (int i = 1; i < registros.Length; i++)
            {
                registro = registros[i];
                tipoObj = typeof(T);
                obj = (T)Activator.CreateInstance(tipoObj);
                campos = registro.Split(separadorCampo);
                for (int j = 0; j < campos.Length; j++)
                {
                    tipoCampo = obj.GetType().GetProperty(cabecera[j]).PropertyType;
                    valor = Convert.ChangeType(campos[j], tipoCampo);
                    obj.GetType().GetProperty(cabecera[j]).SetValue(obj, valor);
                }
                lista.Add(obj);
            }
            return (lista);
        }

        public static string SerializarObjeto<T>(T obj, char separadorCampo)
        {
            StringBuilder sb = new StringBuilder();
            PropertyInfo[] propiedades = obj.GetType().GetProperties();
            string tipo;
            for (int i = 0; i < propiedades.Length; i++)
            {
                tipo = propiedades[i].PropertyType.ToString();
                if (propiedades[i].GetValue(obj, null) != null)
                {
                    if (tipo.Contains("Byte[]"))
                    {
                        byte[] buffer = (byte[])propiedades[i].GetValue(obj, null);
                        sb.Append(Convert.ToBase64String(buffer));
                    }
                    else sb.Append(propiedades[i].GetValue(obj, null).ToString());
                }
                else sb.Append("");
                if (i < propiedades.Length - 1) sb.Append(separadorCampo);
            }
            return sb.ToString();
        }
    }
}
