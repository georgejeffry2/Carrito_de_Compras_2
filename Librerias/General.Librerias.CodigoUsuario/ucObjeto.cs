﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace General.Librerias.CodigoUsuario
{
    public class ucObjeto
    {
        public static void Grabar<T>(T obj, string archivo)
        {
            if (obj != null)
            {
                PropertyInfo[] propiedades = obj.GetType().GetProperties();
                if (propiedades != null)
                {
                    using (StreamWriter sw = new StreamWriter(archivo, true))
                    {
                        foreach (PropertyInfo propiedad in propiedades)
                        {
                            sw.WriteLine("{0} = {1}", propiedad.Name,propiedad.GetValue(obj,null));
                        }
                        sw.WriteLine(new String('_', 100));
                    }
                }
            }
        }
    }
}
